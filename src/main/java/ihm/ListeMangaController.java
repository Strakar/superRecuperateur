package main.java.ihm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import main.java.gestionWeb.Download;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ResourceBundle;

public class ListeMangaController implements Initializable {

    @FXML
    private ListView<String> listeManga;
    private Download dow;

    private static final String[] LISTE_SITE = {"https://www.scan-vf.co/manga-list"};
    private ObservableList mangas = FXCollections.observableArrayList();

    public ListeMangaController() {
    }

    private void miseAJour() {
        dow = new Download(LISTE_SITE[0]);
        try {
            listeToFx(dow.listeManga());
        } catch (NullPointerException e) {
            System.out.println("Pas de manga remonté");
        }
    }

    private void listeToFx(List<String> liste) {
        for (String s : liste) {
            mangas.add(s);
        }
    }

    @FXML
    private void setReset() {

        miseAJour();

        listeManga.setItems(mangas);


    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.setReset();
    }
}
